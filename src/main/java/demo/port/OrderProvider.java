package demo.port;

import java.util.List;

import demo.domain.Order;

public interface OrderProvider {

	public List<Order> getOrders();

	public Order getOrder(Long orderId);

	public Order create(Order order);

	public Order update(Order order);

	public boolean delete(Long id);

	public Order processOrder(Order order);

}
