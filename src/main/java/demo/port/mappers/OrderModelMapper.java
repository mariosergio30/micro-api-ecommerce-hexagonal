package demo.port.mappers;

import org.mapstruct.Mapper;

import demo.domain.Order;
import demo.entities.OrderEntity;

@Mapper(componentModel = "spring")
public interface OrderModelMapper {
	
	Order orderToOrderModel(OrderEntity order);
	OrderEntity orderModelToOrder(Order order);
	
}
