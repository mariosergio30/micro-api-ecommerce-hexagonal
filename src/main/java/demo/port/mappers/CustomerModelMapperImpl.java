package demo.port.mappers;

import org.springframework.stereotype.Component;

import demo.domain.Customer;
import demo.entities.CustomerEntity;

@Component
public class CustomerModelMapperImpl implements CustomerModelMapper {

	@Override
	public Customer customerToCustomerModel(CustomerEntity customerEntity) {
		Customer customerModel = new Customer();
		customerModel.setId(customerEntity.getId());
		customerModel.setFiscalNumber(customerEntity.getFiscalNumber());
		customerModel.setName(customerEntity.getName());
		customerModel.setEmail(customerEntity.getEmail());

		return customerModel;
	}

	@Override
	public CustomerEntity customerModelToCustomer(Customer customerModel) {
		CustomerEntity customerEntity = new CustomerEntity();
		customerEntity.setId(customerModel.getId());
		customerEntity.setFiscalNumber(customerModel.getFiscalNumber());
		customerEntity.setName(customerModel.getName());
		customerEntity.setEmail(customerModel.getEmail());

		return customerEntity;
	}


}
