package demo.port.mappers;

import org.springframework.stereotype.Component;

import demo.api.OrderRest;
import demo.domain.Order;

@Component
public class OrderRestMapperImpl implements OrderRestMapper {

	@Override
	public OrderRest orderModelToOrderRest(Order orderModel) {
		OrderRest orderRest = new OrderRest();
		orderRest.setId(orderModel.getId());
		orderRest.setCode(orderModel.getCode());
		orderRest.setDate(orderModel.getDate());
		orderRest.setTotalValue(orderModel.getTotalValue());
		orderRest.setStatus(orderModel.getStatus());
		orderRest.setCustomer(orderModel.getCustomer());

		return orderRest;
	}

	@Override
	public Order orderRestToOrderModel(OrderRest orderRest) {
		Order orderModel = new Order();
		orderModel.setId(orderRest.getId());
		orderModel.setCode(orderRest.getCode());
		orderModel.setDate(orderRest.getDate());
		orderModel.setTotalValue(orderRest.getTotalValue());
		orderModel.setStatus(orderRest.getStatus());
		orderModel.setCustomer(orderRest.getCustomer());

		return orderModel;
	}

}
