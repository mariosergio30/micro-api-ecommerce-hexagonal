package demo.port.mappers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import demo.domain.Customer;
import demo.domain.Order;
import demo.entities.CustomerEntity;
import demo.entities.OrderEntity;

@Component
public class OrderModelMapperImpl implements OrderModelMapper {

	@Autowired
	private CustomerModelMapper customerModelMapper;
	
	@Override
	public Order orderToOrderModel(OrderEntity orderEntity) {
		Order orderModel = new Order();
		orderModel.setId(orderEntity.getId());
		orderModel.setCode(orderEntity.getCode());
		orderModel.setDate(orderEntity.getDate());
		orderModel.setTotalValue(orderEntity.getTotalValue());
		orderModel.setStatus(orderEntity.getStatus());
		Customer customer = new Customer();
		customer = customerModelMapper.customerToCustomerModel(orderEntity.getCustomer());
		orderModel.setCustomer(customer);
		return orderModel;
	}

	@Override
	public OrderEntity orderModelToOrder(Order orderModel) {
		OrderEntity orderEntity = new OrderEntity();
		orderEntity.setId(orderModel.getId());
		orderEntity.setCode(orderModel.getCode());
		orderEntity.setDate(orderModel.getDate());
		orderEntity.setTotalValue(orderModel.getTotalValue());
		orderEntity.setStatus(orderModel.getStatus());
		CustomerEntity customer = new CustomerEntity();
		customer = customerModelMapper.customerModelToCustomer(orderModel.getCustomer());
		orderEntity.setCustomer(customer);
		return orderEntity;
	}
}
