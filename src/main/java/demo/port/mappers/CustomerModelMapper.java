package demo.port.mappers;

import org.mapstruct.Mapper;

import demo.domain.Customer;
import demo.entities.CustomerEntity;

@Mapper(componentModel = "spring")
public interface CustomerModelMapper {
	
	Customer customerToCustomerModel(CustomerEntity customer);
	CustomerEntity customerModelToCustomer(Customer customer);
	
}
