package demo.port.mappers;

import org.mapstruct.Mapper;

import demo.api.OrderRest;
import demo.domain.Order;

@Mapper(componentModel = "spring")
public interface OrderRestMapper {
	
	OrderRest orderModelToOrderRest(Order order);
	Order orderRestToOrderModel(OrderRest order);
	
}
