package demo.controler;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import demo.api.OrderRest;
import demo.service.RestControlerService;
import io.micrometer.core.annotation.Timed;

@RestController
@RequestMapping("/api")
@Timed("orders")
public class RestControler {

	private RestControlerService restControlerService;

    @Autowired
    public RestControler(RestControlerService restControlerService) {
        this.restControlerService = restControlerService;
    }

	@GetMapping("/orders")
	public ResponseEntity<List<OrderRest>> getOrders() {
		List<OrderRest> orderList = restControlerService.getOrders();

		return ResponseEntity.ok(orderList);
	}
	@GetMapping("/orders/{id}")
	public ResponseEntity<OrderRest> getOrderById(@PathVariable Long id) {
	    OrderRest orderRest = restControlerService.getOrderById(id);
	    return ResponseEntity.ok(orderRest);
	}

	@PostMapping("/orders")
	@ResponseStatus(HttpStatus.CREATED)
	public void createOrder(@RequestBody OrderRest orderDTO) {
		restControlerService.createOrder(orderDTO);
	}

	@PutMapping("/orders/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void updateOrder(@PathVariable Long id, @RequestBody OrderRest orderDTO) {
		restControlerService.updateOrder(id, orderDTO);
	}

	@DeleteMapping("/orders/{id}")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	public void deleteOrder(@PathVariable Long id) {
		restControlerService.deleteOrder(id);
	}

}
