package demo.service;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import demo.api.OrderRest;
import demo.domain.Order;
import demo.port.OrderProvider;
import demo.port.mappers.OrderRestMapper;

@Service
public class RestControlerService {

	@Autowired
	private OrderRestMapper orderMapper;

	@Autowired
	private OrderProvider orderProvider;

	public RestControlerService(OrderProvider orderProvider, OrderRestMapper orderMapper) {
		this.orderProvider = orderProvider;
		this.orderMapper = orderMapper;
	}

	public List<OrderRest> getOrders() {
		List<Order> orders = orderProvider.getOrders();

		List<OrderRest> OrderDTOs = orders.stream().map(orderMapper::orderModelToOrderRest)
				.collect(Collectors.toList());
		return OrderDTOs;

	}

	public OrderRest getOrderById(Long id) {
		Order OrderModel = orderProvider.getOrder(id);
		OrderRest OrderDTO = orderMapper.orderModelToOrderRest(OrderModel);
		return OrderDTO;
	}

	public void createOrder(OrderRest orderDTO) {
		Order orderModel = orderMapper.orderRestToOrderModel(orderDTO);
		orderProvider.create(orderModel);
	}

	public void updateOrder(Long id, OrderRest orderDTO) {
		Order orderModel = orderMapper.orderRestToOrderModel(orderDTO);
		orderModel.setId(id);
		orderProvider.update(orderModel);
	}

	public void deleteOrder(Long id) {
		orderProvider.delete(id);
	}

}
