package demo.adapters;

import java.util.List;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

import demo.domain.Order;
import demo.domain.PaymentOrderDTO;
import demo.entities.OrderEntity;
import demo.port.OrderProvider;
import demo.port.mappers.OrderModelMapper;
import demo.repository.order.OrderRepository;


@Service("OrderProviderDatabase")
public class OrderProviderDatabase implements OrderProvider {


	@Autowired
	private OrderRepository repository;
	

	@Autowired
	private OrderModelMapper orderMapper;
	
	
	@Value("${app.services.payment.uri}")
	private String servicePaymentURI;


	public List<Order> getOrders() {
		List<OrderEntity> orders = this.repository.findAll();

		List<Order> OrderDTOs = orders.stream()
				.map(orderMapper::orderToOrderModel)
				.collect(Collectors.toList());

		return OrderDTOs;
	}


	public Order getOrder(Long orderId) {
		OrderEntity orderEntity = this.repository.findById(orderId).get();
		Order  ordermodel= orderMapper.orderToOrderModel(orderEntity);
		
		return ordermodel;			
	}


	public Order create(Order order) {
		OrderEntity orderEntity = orderMapper.orderModelToOrder(order);
		orderEntity = this.repository.save(orderEntity);
		Order  ordermodel= orderMapper.orderToOrderModel(orderEntity);
		
		return ordermodel;
	}


	public Order update(Order order) {

		OrderEntity orderEntity = orderMapper.orderModelToOrder(order);
		orderEntity = this.repository.save(orderEntity);
		Order  ordermodel= orderMapper.orderToOrderModel(orderEntity);
		
		return ordermodel;
		
	}


	public boolean delete(Long id) {

		this.repository.deleteById(id);

		return true;			
	}

	
	
	//@Transactional
	public Order processOrder(Order order) {
			
		
		/******  create orde *****/
		order.setStatus("SUBMITTED");
		
		OrderEntity orderEntity = orderMapper.orderModelToOrder(order);
		orderEntity = this.repository.saveAndFlush(orderEntity);
		Order  ordermodel= orderMapper.orderToOrderModel(orderEntity);
		
		//OrderModel order = this.repository.findById(orderPost.getId()).get();
				
		
		
		
		/***** call FINANCE service (SYNCHRONOUSLY) *****/
		
		PaymentOrderDTO requestFinance = new PaymentOrderDTO();
		requestFinance.setOrderId(ordermodel.getId());
		requestFinance.setFiscalNumber(ordermodel.getCustomer().getFiscalNumber());
		requestFinance.setValue(ordermodel.getTotalValue());
						
		RestTemplate restTemplate = new RestTemplate();	
		ResponseEntity<PaymentOrderDTO> responseFinance = restTemplate.postForEntity(servicePaymentURI, requestFinance, PaymentOrderDTO.class);		
		
		
		if (responseFinance.getBody().getAproved()) {
			
			// update order status
			ordermodel.setStatus("PAYMENT_APPROVED");
			this.update(ordermodel);
			
			// SEND EMAIL AND SMS TO CUSTOMERS			
			System.out.println("Email Payment: Dear " + ordermodel.getCustomer().getName() + " Paymment for your ordermodel " + ordermodel.getCode() + " was processed.");	
		}
		

		
		/****** call LOGISTIC SERVICE service (SYNCHRONOUSLY)  *****/
		/* TO DO 
		 * 
		 * */		
		boolean callLogisticOk = true;
		
		
		if (callLogisticOk) {
			
			// update order status
			ordermodel.setStatus("DELIVERY_SCHEDULED");
			this.update(ordermodel);
			
			// SEND EMAIL AND SMS TO CUSTOMERS	
			System.out.println("Email Delivery: Dear " + ordermodel.getCustomer().getName() + " Your ordermodel " + ordermodel.getCode() + " was will be delivery before 2022/XX/XX ");
			
		}
		
		
		
		return ordermodel;		
	}

		
	



}
