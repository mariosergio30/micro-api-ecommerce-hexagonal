package demo;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;

import demo.adapters.OrderProviderDatabase;
import demo.port.OrderProvider;

@Configuration
public class BeanConfiguration {
	
	@Bean
	public OrderProvider orderProvider() {
		
		return new OrderProviderDatabase();
	}

}
