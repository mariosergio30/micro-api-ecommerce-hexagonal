package demo.domain;

public class Customer {

	private Long id;

	private String fiscalNumber;

	private String name;

	private String email;

	public Customer() {
		super();
	}

	public Customer(Long id, String fiscalNumber, String name, String email) {
		super();
		this.id = id;
		this.fiscalNumber = fiscalNumber;
		this.name = name;
		this.email = email;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getFiscalNumber() {
		return fiscalNumber;
	}

	public void setFiscalNumber(String fiscalNumber) {
		this.fiscalNumber = fiscalNumber;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

}
