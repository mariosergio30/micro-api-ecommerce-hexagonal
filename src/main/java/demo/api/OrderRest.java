package demo.api;

import java.time.LocalDate;

import com.fasterxml.jackson.annotation.JsonFormat;

import demo.domain.Customer;

//@Getter
//@Setter
//@AllArgsConstructor
//@NoArgsConstructor
public class OrderRest {

	private Long id;

	private String code;

	@JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
	private LocalDate date;

	private Double totalValue;

	private String status;

	private Customer customer;

	public OrderRest() {
		super();
	}

	public OrderRest(Long id, String code, LocalDate date, Double totalValue, String status, Customer customer) {
		super();
		this.id = id;
		this.code = code;
		this.date = date;
		this.totalValue = totalValue;
		this.status = status;
		this.customer = customer;
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public LocalDate getDate() {
		return date;
	}

	public void setDate(LocalDate date) {
		this.date = date;
	}

	public Double getTotalValue() {
		return totalValue;
	}

	public void setTotalValue(Double totalValue) {
		this.totalValue = totalValue;
	}

	public Customer getCustomer() {
		return customer;
	}

	public void setCustomer(Customer customer) {
		this.customer = customer;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

}