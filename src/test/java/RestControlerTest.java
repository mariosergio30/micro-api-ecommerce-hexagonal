import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.when;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.junit.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.junit.jupiter.api.extension.ParameterResolver;
import org.junit.runner.RunWith;
import org.junit.runners.Parameterized;
import org.mockito.Mockito;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;

import demo.api.OrderRest;
import demo.controler.RestControler;
import demo.domain.Customer;
import demo.domain.Order;
import demo.port.OrderProvider;
import demo.port.mappers.OrderRestMapper;
import demo.service.RestControlerService;


@RunWith(Parameterized.class)
@ExtendWith(ParameterResolver.class)
public class RestControlerTest {
    private RestControler restControler;
    private List<Order> orderModelList;
    private List<OrderRest> orderRestList;

    @Parameterized.Parameters
    public static Collection<Object[]> getTestData() {
        Customer customer1 = new Customer(1L, "John Lenon", "jo@sky.com", "02366655588");
        Customer customer2 = new Customer(2L, "Paul MacCartney", "paul@music.com", "02766655599");

        Order order1 = new Order(1L, "ORD001", LocalDate.now(), 5000.0, "PENDING", customer1);
        Order order2 = new Order(2L, "ORD002", LocalDate.now(), 10000.0, "COMPLETED", customer2);

        List<Order> orders = new ArrayList<>();
        orders.add(order1);
        orders.add(order2);

        OrderRest orderRest1 = new OrderRest(1L, "ORD001", LocalDate.now(), 5000.0, "PENDING", customer1);
        OrderRest orderRest2 = new OrderRest(2L, "ORD002", LocalDate.now(), 10000.0, "COMPLETED", customer2);

        OrderProvider orderProvider = Mockito.mock(OrderProvider.class);
        when(orderProvider.getOrders()).thenReturn(orders);

        OrderRestMapper orderMapper = Mockito.mock(OrderRestMapper.class);
        when(orderMapper.orderModelToOrderRest(order1)).thenReturn(orderRest1);
        when(orderMapper.orderModelToOrderRest(order2)).thenReturn(orderRest2);

        RestControlerService restControlerService = Mockito.mock(RestControlerService.class);
        when(restControlerService.getOrders()).thenReturn(Arrays.asList(orderRest1, orderRest2));

        RestControler restControler = new RestControler(restControlerService);

        return Arrays.asList(new Object[][]{{orders, Arrays.asList(orderRest1, orderRest2), restControler}});
    }


    public RestControlerTest(List<Order> orderModelList, List<OrderRest> orderRestList, RestControler restControler) {
        this.orderModelList = orderModelList;
        this.orderRestList = orderRestList;
        this.restControler = restControler;
    }

    @Test
    public void shouldGetOrders() {
        ResponseEntity<List<OrderRest>> responseEntity = restControler.getOrders();
        assertEquals(HttpStatus.OK, responseEntity.getStatusCode());

        List<OrderRest> orders = responseEntity.getBody();

        assertEquals(orderRestList.size(), orders.size());

        assertEquals(orderRestList.get(0).getId(), orders.get(0).getId());
        assertEquals(orderRestList.get(1).getId(), orders.get(1).getId());
    }
}

